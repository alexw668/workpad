###############################################################
# Filename:    html-mode.dm
# By:          Liyun Wang, GSFC/ARC, January 19, 1995
#
# Last Modified: Tue Dec 10 11:38:20 1996 (lwang@achilles.nascom.nasa.gov)
###############################################################
#

#######
# MODE:	html-helper-mode html-mode
#######
# ALIAS: html_tail	(dmacro tail)

#######
htmlhead	expand	Header of HTML files.
<!-- 
---------------------------------------------------------------------
Document name: ~(file)
Created on:    ~(chron)
Created by:    ~(user-name)~(organization)
Last Modified: ~(mark)
---------------------------------------------------------------------
-->
<html>
<head>
<title>~(prompt title "Enter title of the page: ")</title>

</head>
<body>
<center>
<h1>~(prompt)</h1><br>
</center>

~(point)

~(html_tail)

<!--
---------------------------------------------------------------------
End of '~(file)'
---------------------------------------------------------------------
-->
#
#######
tail expand Insert tail part of an HTML file.
<hr>
<address>
<B>Author:</B> <br> 
<a href="http://orpheus.nascom.nasa.gov/~lwang">Liyun Wang</a> 
(<a href="mailto:Liyun.Wang.1@gsfc.nasa.gov">Liyun.Wang.1@gsfc.nasa.gov</a>)<p>

<B>Responsible NASA Official:</B><br>
Arthur I. Poland, Code 682.1 
(<a href="mailto:Arthur.I.Poland.1@gsfc.nasa.gov">Arthur.I.Poland.1@gsfc.nasa.gov</a>)<p>

<B>Last revised:</B> ~(today)<p>
</address>
</body>
</html>

#
#######
comment expand Insert a pair of tags for comment
<!--------------------------------------------------------------------
 ~(point)
--------------------------------------------------------------------->

#
#######
b expand Insert the tag for bold face in an HTML file.
<b>~point</b>
#
#######
i expand Insert the tag for italic font in an HTML file.
<i>~point</i>
#
#######
a expand Insert the tag of an ancor in an HTML file.
<a href=~point>~mark</a>
#
#######
h1 expand Insert the 1st level head in an HTML file.
<h1>~point</h1>
#
#######
h2 expand Insert the 2nd level head in an HTML file.
<h2>~point</h2>
#
#######
h3 expand Insert the 3rd level head in an HTML file.
<h3>~point</h3>
#
#######
h4 expand Insert the 4th level head in an HTML file.
<h4>~point</h4>
#
#######
h5 expand Insert the 5th level head in an HTML file.
<h5>~point</h5>
#
#######
h6 expand Insert the 6th level head in an HTML file.
<h6>~point</h6>
#
#######
ul expand Unordered List
<ul>
~point
</ul>

#
#######
ol expand Ordered List
<ol>
~point
</ol>

#

