"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => General Settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" set leader key
let g:mapleader = "\<Space>"

syntax enable                           " Enables syntax highlighing
syntax on
set hidden                              " Required to keep multiple buffers open multiple buffers
set nowrap                              " Display long lines as just one line
set encoding=utf-8                      " The encoding displayed
set pumheight=10                        " Makes popup menu smaller
set fileencoding=utf-8                  " The encoding written to file
set ruler                			       " Show the cursor position all the time
set cmdheight=2                         " More space for displaying messages
set iskeyword+=-                        " treat dash separated words as a word text object"
set mouse=a                             " Enable your mouse
set splitbelow                          " Horizontal splits will automatically be below
set splitright                          " Vertical splits will automatically be to the right
set t_Co=256                            " Support 256 colors
set conceallevel=0                      " So that I can see `` in markdown files
set tabstop=3 softtabstop=3             " Insert 3 spaces for a tab
set shiftwidth=3                        " Change the number of space characters inserted for indentation
set smarttab                            " Makes tabbing smarter will realize you have 2 vs 4
set expandtab                           " Converts tabs to spaces
set smartindent                         " Makes indenting smart
set autoindent                          " Good auto indent
set laststatus=2                        " Always display the status line
set number relativenumber               " Show (relativenumber) Line numbers
set cursorline                          " Enable highlighting of the current line
set background=dark                     " tell vim what the background color looks like
set showtabline=1                       " 0: never; 1: only if there are at least two tabs; 2: Always show tabs
set noshowmode                          " We don't need to see things like -- INSERT -- anymore
set nobackup                            " This is recommended by coc
set nowritebackup                       " This is recommended by coc
set updatetime=300                      " Faster completion
set timeoutlen=500                      " By default timeoutlen is 1000 ms
set formatoptions-=cro                  " Stop newline continution of comments
set clipboard=unnamedplus               " Copy paste between vim and everything else
set scrolloff=8                         " Set scroll off limit

set path+=**					             " Searches current directory recursively.
set wildmenu					             " Display all matches when tab complete.

set hlsearch 
set incsearch                           " Incremental search
if has('nvim')
   set inccommand=nosplit
endif  
set smartcase
set ignorecase
set nobackup                            " No auto backups
set hidden                              " Needed to keep multiple buffers open
set noswapfile                          " No swap
let g:rehash256 = 1

set noerrorbells
set nowrap
set smartcase
set undofile
set undodir=~/.vim/undodir

set colorcolumn=80
set cpoptions+=$

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" => Splits and Tabbed Files
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set splitbelow splitright

highlight ColorColumn ctermbg=0 guibg=lightgrey

"if has('nvim')
"   colorscheme gruvbox
"   set background=dark
"endif

if executable('rg')
   let g:rg_derive_root='true'
endif

" auto wrapping lines at 80th column
if has('autocmd')
  au BufRead,BufNewFile *.txt set wm=2 tw=80
endif

let g:ctrop_user_command = ['.git', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']
let g:ctrlp_use_caching= 0

augroup Format-Options
    autocmd!
    autocmd BufEnter * setlocal formatoptions-=c formatoptions-=q 
             \ formatoptions-=n formatoptions-=r formatoptions-=o 
             \ formatoptions-=l formatoptions+=j
augroup END

augroup exe_code
   autocmd!

   " Execute python code
   autocmd FileType python nnoremap <buffer> <localleader>r
      \ :sp<CR> :term python3 %<CR> :startinsert<CR>

   " Exceute javascript code
   autocmd FileType javascript nnoremap <buffer> <localleader>r
      \ :sp<CR> :term nodejs %<CR> :startinsert<CR>

   " Execute bash code
   autocmd FileType bash,sh nnoremap <buffer> <localleader>r
      \ :sp<CR> :term bash %<CR> :startinsert<CR>
augroup END

"You can't stop me
" cmap w!! w !sudo tee %
