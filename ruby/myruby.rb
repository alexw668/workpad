require 'json'
class MyRuby
  class << self
    @@report_every = 100
    @@root = '/home/alex/FlightLink/RpmOnline'
    @@verbose = false
    def do_it( sync_file='RN_Message3.sync')
      file = File.join(@@root,"db",sync_file)
      File.open(file, "r") do |f|
        cur = 0
        early = Time.now
        f.each_line do |line|
          message = JSON.parse(line)
          if cur == 0
            now = Time.now
            diff = now - early
            puts "Took #{diff} to start doing the job"
          end
          cur += 1
          report_process cur
        end
        puts "Total of #{cur} line read."
      end
    end

    def do_it2( sync_file='RN_Message3.sync')
      file = File.join(@@root,"db",sync_file)
      File.open(file, "r") do |f|
        cur = 0
        early = Time.now
        while (line = f.gets)
          if cur == 0
            now = Time.now
            diff = now - early
            puts "Took #{diff} to start doing the job"
          end
          message = JSON.parse(line)
          cur += 1
          report_process cur
        end
        puts "Total of #{cur} line read."
      end
    end


    def time_period_to_s time_period       
      out_str = ''
     
      interval_array = [ [:weeks, 604800], [:days, 86400], [:hours, 3600], [:mins, 60] ]
      interval_array.each do |sub|
        if time_period>= sub[1] then
          time_val, time_period = time_period.divmod( sub[1] )
         
          time_val == 1 ? name = sub[0].to_s.singularize : name = sub[0].to_s
           
          ( sub[0] != :mins ? out_str += ", " : out_str += " and " ) if out_str != ''
          out_str += time_val.to_s + " #{name}"
        end
      end
 
      return out_str 
    end

    private
 
    def report_process(cur)
      if @@verbose
        if (cur % @@report_every) == 0
          print "#{cur}" 
        else
          print "."
        end
      end
    end
  end
end

module ActiveSupport
  module Inflector
    class Inflections
      @@acronyms = ['TAR','SAR','CCS']
      def titleize(word)
        puts "helloe"
        aa = humanize(underscore(word)).gsub(/\b('?[a-z])/) { $1.capitalize }.split(/\s+/)
        aa.map!{ |e| @@acronyms.include?(e.upcase) ? e.upcase : e}
        aa.join(' ')
      end
    end
  end
end
