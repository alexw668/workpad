# Macros for use with Dmacro in ruby mode, modified from Dmacro's demo.dm

# ALIAS: dy	((chron) 22)
# ALIAS: dd	((chron) 8 10 :pad nil)
# ALIAS: dn	((month-num) :pad nil)

#######
# MODE:	ruby-mode 
#######
#######
b	indent	curly braces (ideal for dmacro-wrap-line)
{
~@
}
#
#######
ife	indent	if/else statement
if ~@
 ~(mark)
else
 ~(mark)
end

#
#######
if	indent	if statement
if ~@
 ~(mark)
end

#
#######
while	indent	while statement
while (~@) do
 ~mark
end

#
#######
case	indent	switch statement
case ~(prompt variable)
   when ~@ then ~(mark) 
   else ~(mark)
end
#
#######
class	indent insert class def
class ~(prompt class_name)
   ~@
end

#
#######
module	indent insert module def
module ~(prompt module_name)
   ~@
end

#
#######
unless indent Insert unless structure in Perl script
unless ~(prompt condition)
   ~(point)
end

#
#######
each indent Insert the each structure
each do |~(prompt variable)|
    ~(point)
end

#
#######
try indent Insert begin-rescue block
begin
   ~(point)
rescue Exception => ex

end

#
#######
describe indent Insert describe block
describe "~(prompt what)" do
  before :each do
  end

  it "~(prompt should_do_what)" do
    ~(point)
  end
end

#
#######
it indent Insert it example block
it "~(prompt shouod_do_what)" do
  ~(point)
end

#
