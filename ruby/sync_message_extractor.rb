#
# A utility class that can be used for extracting sync message from an existing billing record in db
# 
# Usage (in Rails console):
#   > load 'db/sync_message_extractor.rb'
#   > message = SyncMessageExtractor.get_message '11-22123'
#   or
#   > hash = SyncMessageExtractor.get_message '11-22123', true
#
# With extracted and modified message, you can send it again
#   > SyncMessageExtractor.send_message message
#   > SyncMessageExtractor.send_message hash.to_json
#
class SyncMessageExtractor
  class << self    
    def get_message(account_number, as_hash=false)
      TenantPrivacy.disable!
      source_system_id='hsi'
      billing_record = BillingRecord.first :billing_record_number=>account_number, :source_system_id=>source_system_id
      return unless billing_record    
      msg = {}
      msg["SourceSystem_ID"] = billing_record.source_system_id    
      msg["BillingRecord"] = populate_billing_record billing_record
      return msg if as_hash
      msg.to_json
    end

    def send_message(message)
      TenantPrivacy.disable!
      Messaging::PusherMessenger.shut_up!
      #ExternalDataSyncListener.preload_sync_data_cache
      ExternalDataSyncListener.begin_bulk_sync!
      message_hash = JSON.parse(message)
      event_path = get_event_name message_hash
      puts "event message to send: event_path: #{event_path} - message: #{message_hash.inspect}"                      
      ExternalDataSyncListener.process_message_inner(event_path,message)
      Messaging::PusherMessenger.i_cant_hear_you!
      ExternalDataSyncListener.end_bulk_sync!
    end

    private
    def populate_billing_record(b)    
      account_id = "%010d" % Kernel.rand(99999999)
      bhash = {}
      bhash["BillingRecord_ID"] = account_id
      bhash["CurrentSchedule_ID"] = b.current_schedule.global_id if b.current_schedule
      bhash["Patient_ID"] = b.patient.global_id if b.patient
      bhash["SourceSystemRecordNumber"] = b.billing_record_number
      bhash["DateOfService"] = b.date_of_service.strftime("%Y-%m-%dT%H:%M:%SZ")
      bhash["CadRecordID"] = b.cad_record_id
      bhash["TypeOfService_ID"] = SyncData.lookup_id_by_name(:type_of_service,b.type_of_service)
      bhash["ProfitCenter_ID"] = b.profit_center.global_id if b.profit_center
      payer = Payer.first :id=>b.current_payer["payer_id"] if b.current_payer
      bhash["CurrentPayer_ID"] = payer.global_id if payer
      payer = Payer.first :id=>b.primary_payer["payer_id"] if b.current_payer
      bhash["PrimaryPayer_ID"] = payer.global_id if payer
      bhash["LoadedMiles"] = b.loaded_miles
      bhash["TotalMiles"] = b.total_miles
      bhash["TotalCredits"] = b.total_credits
      bhash["TotalCharges"] = b.total_charges
      bhash["Balance"] = b.current_balance
      bhash["PickupFacility_ID"] = b.pickup_facility.global_id if b.pickup_facility
      bhash["ReceivingFacility_ID"] = b.receiving_facility.global_id if b.receiving_facility
      bhash["IsConsentFormOnFile"] = b.is_consent_form_on_file
      bhash["IsDeleted"] = false
      bhash["Credit"] = populate_credits(b.credits, account_id)
      bhash["Charge"] = populate_charges(b.charges, account_id)
      bhash
    end

    def populate_credits(credits, account_id)
      result = []
      credits.each do |credit|
        result << { "Credit_ID"=>credit[:credit_id], 
                    "BillingRecord_ID"=>account_id,
                    "CreditType_ID"=>credit[:credit_type][:credit_type_id],
                    "CreditReceivedDate"=>credit[:credit_received_date].strftime("%Y-%m-%dT%H:%M:%SZ"),
                    "CreditEnteredDate"=>credit[:credit_entered_date].strftime("%Y-%m-%dT%H:%M:%SZ"),
                    "EffectiveDate"=>credit[:effective_date].strftime("%Y-%m-%dT%H:%M:%SZ"),
                    "Amount"=>credit[:amount],
                    "User_ID"=>credit[:user_id],
                    "Payer_ID"=>credit[:payer_global_id],
                    "CreditCode_ID"=>credit[:credit_code_id],
                    "IsDeleted"=>false
                  }
      end      
      result
    end

    def populate_charges(charges, account_id)
      result = []
      charges.each do |charge|
        result << {"Charge_ID"=>charge[:charge_id], 
                  "BillingRecord_ID"=>account_id,
                  "ChargeType_ID"=>charge[:charge_type][:charge_type_id],
                  "ChargeEnteredDate"=>charge[:charge_entered_date].strftime("%Y-%m-%dT%H:%M:%SZ"),
                  "EffectiveDate"=>charge[:effective_date].strftime("%Y-%m-%dT%H:%M:%SZ"),
                  "Amount"=>charge[:amount],
                  "UnitCost"=>charge[:unit_cost],
                  "User_ID"=>charge[:user_id],
                  "Quantity"=>charge[:quantity],
                  "ChargeCode_ID"=>charge[:charge_code_id],
                  "Hcpcs"=>charge[:hcpcs],
                  "IsDeleted"=>false
                  }
      end
      result
    end
  end
end