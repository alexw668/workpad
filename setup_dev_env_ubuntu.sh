#!/usr/bin/env bash

get_yes_answer() {
    read -p "$1 Are you sure [Y/n]? " answer
    if [ -n $answer ] && [[ $answer == "n" || $answer == "N" ]]; then
        return 1
    fi
}

get_no_answer() {
    read -p "$1 Are you sure [y/N]? " answer
    if [ -n $answer ] && [[ $answer == "y" || $answer == "Y" ]]; then
        return 1 fi }

sudo apt-get update

if ! $(get_no_answer "install previously used stuff like mongodb, emacs, etc."); then
	#Mongodb
	#Mongob: use command on packages page to add repositories
	#edit sources list to remove deb-src line
	sudo apt-add-repository 'deb http://downloads.mongodb.org/distros/ubuntu 10.10 10gen'

	# or edit /etc/apt/sources.list by adding the following line
	deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen

	#  import the PGP key into apt's public keyring 
	sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10

	sudo apt-get install mongodb-stable

	# emacs
	sudo apt-get install emacs

	# Get xdotool (for simulating X11 keyboard/mouse input)
	sudo apt-get install xdotool

	# xclip (to be used for copy/paste)
	sudo apt-get install xclip

	# Meld (diff and merge tool)
	sudo apt-get install meld

	# ack-grep
	sudo apt-get install ack-grep

fi

if ! $(get_no_answer "Install Ruby related stuff."); then
	#Rabbitmq
	sudo apt-get install rabbitmq-server

	#Ruby
	sudo apt-get install ruby-full

	#Ruby gems
	wget production.cf.rubygems.org/rubygems/rubygems-1.3.7.tgz

	tar -xvf rubygems-1.3.7.tgz

	cd rubygems-1.3.7/

	ruby setup.rb

	sudo ln -s /usr/bin/gem1.8 /usr/bin/gem

	# nginx (for staging/production env)
	# sudo aptitude install nginx

	# or better install nginx with Phusion Passenger 
	sudo gem install passenger
	passenger-install-nginx-module

	#Various required libs
	sudo apt-get install curl bison build-essential autoconf zlib1g-dev libssl-dev libxml2-dev libreadline6-dev git-core htop libxml2 libxml2-dev libxslt1-dev

	sudo gem install bundler

	#CoffeeScript
	npm install coffee-script	
fi

if $(get_yes_answer "Install ripgrep (aka rg) and silversearch-ag (aka ag)"); then
	sudo apt-get install ripgrep
	sudo apt install silversearcher-ag
fi

if $(get_yes_answer "Install highlight"); then
	sudo apt install highlight
fi

if $(get_yes_answer "Install and setup nervim"); then
	sudo apt-get install neovim
  	sudo apt install xsel

  	mkdir -p ~/.config/nvim
  	touch ~/.config/nvim/init.vim

  	# install vim-plug
  	curl -fLo ~/.config/nvim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  	mkdir -p ~/.config/nvim/vim-plug
  	touch ~/.config/nvim/vim-plug/plugins.vim
fi

if ! $(get_no_answer "Create SSH key."); then
	# create SSH key
	cd ~/
	mkdir .ssh
	ssh-keygen -t rsa -C "your_email@youremail.com"
fi

if ! $(get_no_answer "Install NodeJS && npm"); then
	#NodeJS && NPM
	echo 'export PATH=$HOME/local/bin:$PATH' >> ~/.bashrc
	. ~/.bashrc‫
	mkdir ~/local
	mkdir ~/node-latest-install
	cd ~/node-latest-install
	curl http://nodejs.org/dist/node-latest.tar.gz | tar xz --strip-components=1
	./configure --prefix=~/local
	make install
	#Restart your console after this step
	curl http://npmjs.org/install.sh | sh
	#Restart your console after this step too
fi
