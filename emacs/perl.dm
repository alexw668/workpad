###############################################################
# Filename:    perl.dm
# By:          Liyun Wang, GSFC/ARC, January 19, 1995
#
# Last Modified: Tue Mar 20 10:47:38 2001 (awang@saturn)
###############################################################
#
# Macros for use with Dmacro in perl mode
#######
# MODE:	perl-mode
#######
header expand Insert a standard header on the perl script document
\#!/usr/local/bin/perl -- # -*-Perl-*-
\#------------------------------------------------------------------
\# Filename:      ~(file)
\# Created on:    ~(chron)
\# By:            ~(user-name)~(organization)
\# Last Modified~(mark): 
\#------------------------------------------------------------------
\#
\# Purpose:
\#     ~(point)
\#
\# Modification History:
\#     Version 1, created, ~(user-name)~(organization), ~(today)
\#
\# Contact:
\#     ~(user-name)~(organization) (~(my_email))
\#------------------------------------------------------------------
\#

while ( $ARGV[0] =~~ /^-(.*)/ && shift ) 
{
    if ( $1 =~~ /^\h/ ) {&usage;}
    elsif ( $1 =~~ /^v/i ) { $VERBOSE = 1; }
    elsif ( $1 =~~ /^c/i ) { $CONFIRM = 1; }
    else { print "Invalid option -$1 ignored.\n\n"; }
}

&usage unless ( @ARGV );

&action( @ARGV );

exit 0;

sub action
{
\#------------------------------------------------------------------
\# Make your code here
\#------------------------------------------------------------------
    my (@list) = @_;
    ~(mark)
}

sub usage
{
    print "Usage:\n\n";
    print "    ~(file-name) [options] TBD\n";
    print "Options:\n";
    print "    -h    Print this help message\n";
    print "    -v    Run in verbose mode\n";
    print "    -c    Confirm before each process\n";
    exit 1;
}

\#------------------------------------------------------------------
\# End of '~(file)'
\#------------------------------------------------------------------
#
#######
comment expand Insert commentary structure in Perl script
\#------------------------------------------------------------------
\# ~(point)
\#------------------------------------------------------------------

#
#######
md expand Insert modification history
\#     Version ~(prompt version), ~(user-name)~(organization), ~(today)
\#        ~(point)

#
#######
if indent Insert if ... else ... endif structure in Perl script
if ( ~(prompt condition) ) 
{
    ~(point)
}

#
#######
ifelse indent Insert if ... else ... structure in Perl script
if ( ~(prompt condition) ) 
{
    ~(point)
} 
else 
{
    ~(mark)
}

#
#######
ifelsif indent Insert if ... elsif ... else ... structure in Perl script
if ( ~(prompt condition) ) 
{
    ~(point)
} 
elsif ( (~mark) ) 
{

} 
else 
{
    ~(mark)
}

#
#######
foreach indent Insert the foreach structure in Perl script
foreach ( ~(prompt variable) ) 
{
    ~(point)
}

#
#######
for indent Insert the for structure in Perl script
for ( ~(prompt expr1); ~(prompt expr2); ~(prompt expr3) ) 
{
    ~(point)
}

#
#######
ifor indent Insert the for structure in Perl script
for ( $~(prompt variable) = 0; ~(prompt condition); $~(prompt variable)++ ) 
{
~(point)
}

#
#######
while indent Insert while structure in Perl script
while ( ~(prompt condition) ) 
{
    ~(point)
}

#
#######
unless indent Insert unless structure in Perl script
unless ( ~(prompt condition) ) 
{
   ~(point)
}

#
#######
do indent Insert do ... until structure in Perl script
do 
{
   ~(point)
} until ( ~(prompt condition) );

#
#######
sub indent Insert sub structure in Perl script
sub ~(prompt name) 
{
\#-----------------------------------------------------------------------
\#   Purpose:
\#       ~(point)
\#
\#   Usage:
\#       &~(prompt)();
\#
\#   External subprogram(s) required: 
\#       None
\#-----------------------------------------------------------------------
    my( ~(mark) ) = @_;
}

#
#######
hl indent Insert the escape sequence for highlighting
\e[1m~(point)\e[0m
#
