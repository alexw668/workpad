;; auto-indent.el -- Set up minor mode to auto-indent relatively.

;; Install this file somewhere in your load-path.
;; Put "(autoload 'auto-indent-mode "auto-indent") in your ".emacs" file.

;; Created by,
;; Alan K. Stebbens  7 Aug 87

;; Added minor maps and ugly code by,
;; Per Abrahamsen at University of Aalborg, Denmark 12 sep 87.
;; mail amanda@iesd.uucp

;; This file is hopefully to small to be affected by any form of copyright.

(require 'minor-map)

(defvar auto-indent-flag nil "If set, indicates that auto-indent mode
is active.  This variable is automatically set by invoking
\\[auto-indent-mode].")

(make-variable-buffer-local 'auto-indent-flag)
(set-default 'auto-indent-flag nil)

(make-variable-buffer-local 'old-indent-line)
(set-default 'old-indent-line nil)

(setq minor-mode-alist (cons '(auto-indent-flag " Indent") 
			     minor-mode-alist))

(defun auto-indent-mode (arg) "\
Toggle auto indent mode.
With arg, turn auto indent mode on iff arg is positive.

Auto-indent mode works by invoking indent-relative for TAB,
and using indent-relative-maybe as the indent-line-function for auto-fill,
and NEWLINE."  
  (interactive "P")
  (if (or (and (null arg) auto-indent-flag)
	  (<= (prefix-numeric-value arg) 0))
      (if auto-indent-flag
	  (progn			;Turn it off
	    (unbind-minor-mode 'auto-indent-mode)
	    (setq indent-line-function old-indent-line)
	    (setq auto-indent-flag nil)))
    (if auto-indent-flag		;Turn it on
	()
      (minor-set-key "\t" 'indent-relative 'auto-indent-mode)
      (minor-set-key "\n" 'newline-and-indent 'auto-indent-mode)
      (setq old-indent-line indent-line-function)
      (setq indent-line-function 'indent-relative-maybe)
      (setq auto-indent-flag t)))
  (set-buffer-modified-p (buffer-modified-p)))






