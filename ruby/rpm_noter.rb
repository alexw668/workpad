#!/usr/bin/ruby
require 'rubygems'
require 'cgi'
require 'rexml/document'
require 'rexml/streamlistener'
require 'hpricot'
require 'net/http'
require 'uri'

class ReleaseNoter
	include REXML

	@@project_id = 194309
	@@token = '433b1f98512a7bfe96857531c6c84aca'

	def self.story(story_number)
		begin
      # cmd = 'curl -H "X-TrackerToken: $TOKEN" -X GET http://www.pivotaltracker.com/services/v3/projects/$PROJECT_ID/stories?filter=current_state%3A%22finished%22'
			cmd = `curl -s -H "X-TrackerToken: #{@@token}" -X GET http://www.pivotaltracker.com/services/v3/projects/#{@@project_id}/stories/#{story_number}`
			xmldoc = Document.new(cmd)
			create_html xmldoc.elements["story"]			
		rescue Exception => e
			puts "Error occurred: #{e.message}"
		end
	end

  # requiring installation of hpricot gem
  def self.story2(story_number)
    begin

      resource_uri = URI.parse("http://www.pivotaltracker.com/services/v3/projects/#{@@project_id}/stories/#{story_number}")
      response = Net::HTTP.start(resource_uri.host, resource_uri.port) do |http|
        http.get(resource_uri.path, {'X-TrackerToken' => "#{@@token}"})
      end
      doc = Hpricot(response.body).at('story')
      story = {
        :name => doc.at('name').innerHTML,
        :url => doc.at('url').innerHTML,
      }
      puts "<li><a href='#{story[:url]}'>#{story[:name]}</a></li>"
    rescue Exception => e
      puts "Error occurred: #{e.message}"
    end
  end

	def self.create_html(node)
		name = CGI::escapeHTML(node.elements["name"].text)
		url = node.elements['url']
		puts "<li><a href='#{url.text}'>#{name}</a></li>"
	end
end

if ARGV[0]
	ReleaseNoter.story2(ARGV[0]) 
else
	puts "Usage: release_noter <pivotal story id>"
end
