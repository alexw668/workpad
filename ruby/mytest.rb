class MyTest

  def initialize(type=:money, alignment=:left, padding=:zero)
    @type = type
    @alignment = alignment
    @padding = padding
  end

  @@data = []
  
  def self.reset
    @@data = []
  end

  def append data
    @@data << data
  end

  def self.output
    @@data
  end

  def add_data(data)
    append data
  end

  def format_money(value)
     ali = @padding == :zero ? '0' : ( @alignment == :right ? '' : '-' )
     format = @type == :money_with_implied_decimal ? "%#{ali}#{sizer}d" : "%#{ali}#{sizer}.2f"
     value = 100 * value.to_f if @type == :money_with_implied_decimal       
     result = format % value
  end 
         
  def sizer
    11
  end
end

a = "100011212"
case a
  when /^0+/ then puts "hello"
  else puts "bye!"
end
