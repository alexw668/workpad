#!/usr/bin/ruby

require "rubygems"

puts "Loading IRB..."

# def start_ap
#     require "ap"
#     IRB::Irb.class_eval do
#         def output_value
#             ap @context.last_value
#         end
#     end
# end

# def stop_ap
#     IRB::Irb.class_eval do
#         def output_value
#             puts "#{@context.last_value.inspect}"
#         end
#     end
# end

class Object
  # list methods which aren't in superclass
  def local_methods(obj = self)
    (obj.methods - obj.class.superclass.instance_methods).sort
  end

  def my_methods
    (methods - Object.public_instance_methods).sort
  end

  # print documentation
  #
  # ri 'Array#pop'
  # Array.ri
  # Array.ri :pop
  # arr.ri :pop
  def ri(method = nil)
    unless method && method =~ /^[A-Z]/ # if class isn't specified
      klass = self.kind_of?(Class) ? name : self.class.name
      method = [klass, method].compact.join('#')
    end
    system 'ri', method.to_s
  end

# Print Documentation
# Example: String.ri2 :sub
# Source: http://github.com/ryanb/dotfiles/blob/145906d11810c691dbb1a47481d790e3ad186dcb/irbrc
  def ri2(method = nil)
    unless method && method =~ /^[A-Z]/ # if class isn't specified
      klass = self.kind_of?(Class) ? name : self.class.name
      method = [klass, method].compact.join('#')
    end
    puts `ri '#{method}'`
  end

end

# Where quasi global methods belong (Thanks to aperios for teaching me!)
module Kernel

  # Awesome benchmarking function
  # Example: bench(100) do ... end
  # Source: http://ozmm.org/posts/time_in_irb.html
  def time(times=1)
    require "benchmark"
    ret = nil
    Benchmark.bm { |x| x.report { times.to_i.times { ret = yield } } }
    ret
  end
  alias :bench :time

  # Simple regular expression helper
  # show_regexp - stolen from the pickaxe
  def show_regexp(a, re)
    if a =~ re
      "#{$`}<<#{$&}>>#{$'}"
    else
      "no match"
    end
  end

  # using xclip to do copy/paste to/from clicpboard
  def copy(str)
    IO.popen('xclip -i', 'w') { |f| f << str.to_s }
  end

  def paste
    `xclip -o`
  end

  # For MacOS
  # def pbcopy
  #   IO.popen('pbcopy', 'w') { |io| io.write(IRB.CurrentContext.last_value.to_s) }
  # end
end

# A real array diff, not a set diff
# a1 = [1,1,2]
# a2 = [1,2]
# a1 - a2 #=> []
# a1.diff(a2) #=> [1]
class Array
  def diff(other)
    list = self.dup
    other.each { |elem| list.delete_at( list.index(elem) ) }
    list
  end
end

# start_ap

# Not to use any encoding
$KCODE=""

# Tab Completion
require "irb/completion"

# Automatic Indentation
IRB.conf[:AUTO_INDENT]=true

# Save History between irb sessions
require 'irb/ext/save-history'
IRB.conf[:SAVE_HISTORY] = 100
IRB.conf[:HISTORY_FILE] = "#{ENV['HOME']}/.irb-save-history"

# Remove the annoying irb(main):001:0 and replace with >>
IRB.conf[:PROMPT_MODE]  = :SIMPLE

# Syntax highlighting (to work with Rails 3, wirble needs to be added in Gemfile)
require 'rubygems'
#%x{gem install 'wirble' --no-ri --no-rdoc} unless Gem.available?('wirble')
#Gem.refresh
require 'wirble'

Wirble.init
Wirble.colorize

colors = Wirble::Colorize.colors.merge({
 :object_class => :purple,
 :symbol => :purple,
 :symbol_prefix => :purple
})
Wirble::Colorize.colors = colors

# Clear the screen
def clear
  system 'clear'
  if ENV['RAILS_ENV']
    return "Rails environment: " + ENV['RAILS_ENV']
  else
    return "No rails environment - happy hacking!";
  end
end

# Shortcuts
alias cls clear

def seconds_to_s time_period
  out_str = ''

  interval_array = [ [:weeks, 604800], [:days, 86400], [:hours, 3600], [:mins, 60] ]
  interval_array.each do |sub|
    if time_period>= sub[1] then
      time_val, time_period = time_period.divmod( sub[1] )

      time_val == 1 ? name = sub[0].to_s.singularize : name = sub[0].to_s

      ( sub[0] != :mins ? out_str += ", " : out_str += " and " ) if out_str != ''
      out_str += time_val.to_s + " #{name}"
    end
  end

  return out_str
end

#########################
ANSI_BOLD       = "\033[1m"
ANSI_RESET      = "\033[0m"
ANSI_LGRAY    = "\033[0;37m"
ANSI_GRAY     = "\033[1;30m"
ANSI_BLUE     = "\033[1;33m"
ANSI_RED     = "\033[1;32m"

class Object
  def pm(*options) # Print methods
    methods = self.methods
    methods -= Object.methods unless options.include? :more
    filter = options.select {|opt| opt.kind_of? Regexp}.first
    methods = methods.select {|name| name =~ filter} if filter

    data = methods.sort.collect do |name|
      method = self.method(name)
      if method.arity == 0
        args = "()"
      elsif method.arity > 0
        n = method.arity
        args = "(#{(1..n).collect {|i| "arg#{i}"}.join(", ")})"
      elsif method.arity < 0
        n = -method.arity
        args = "(#{(1..n).collect {|i| "arg#{i}"}.join(", ")}, ...)"
      end
      klass = $1 if method.inspect =~ /Method: (.*?)#/
      [name, args, klass]
    end
    max_name = data.collect {|item| item[0].size}.max
    max_args = data.collect {|item| item[1].size}.max
    data.each do |item|
      print "#{ANSI_LGRAY}#{item[0].rjust(max_name)}#{ANSI_RESET}"
      print "#{ANSI_BLUE}#{item[1].ljust(max_args)}#{ANSI_RESET}"
      print "#{ANSI_RED}#{item[2]}#{ANSI_RESET}\n"
    end
    data.size
  end
end
#########################

if ENV['RAILS_ENV']
  rails_env = ENV['RAILS_ENV']
  rails_root = File.basename(Dir.pwd)
  prompt = "#{rails_root}[#{rails_env.sub('production', 'prod').sub('development', 'dev')}]"
  IRB.conf[:PROMPT] ||= {}

  IRB.conf[:PROMPT][:RAILS] = {
    :PROMPT_I => "#{prompt}>> ",
    :PROMPT_S => "#{prompt}* ",
    :PROMPT_C => "#{prompt}? ",
    :RETURN   => "=> %s\n"
  }

  IRB.conf[:PROMPT_MODE] = :RAILS
end
