#!/usr/bin/env bash

alias a='alias'

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    a ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    a grep='grep --color=auto'
    a fgrep='fgrep --color=auto'
    a egrep='egrep --color=auto'
fi

# some more aliases
a cls='clear'
if [ -x /usr/bin/exa ]; then
    alias ls='exa'
    alias ll='exa -alh'
    alias la='exa -a'
    alias tree='exa --tree'
else
    alias ll='ls -l'
    alias la='ls -A'
    alias l='ls -CF'
fi
a pop='cd ..'
a pd='pushd'
a home='cd ~/'
a k9='kill -9'
a jobs='jobs -l'
a no~='rm *~'
a h='history'
a histg='history | grep $1'  # used to be hg, but hg on Mac OS can mean Mercurial (a distributed source control management tool)
a ..='cd ..'
a ...='cd ../..'
a ....='cd ../../..'
a j='jobs'
a peek='less -X'
a less2='less -X'
a reload='source ~/.zshrc'

a nv='nvim'
a v='vim'

# Require highlight being installed
if [ -x /usr/bin/highlight ]; then
	a dog='highlight -O ansi --force'
fi

# Reset terminal title and icon label to Terminal
a rtt='echo -en "\033]2;Terminal\007"; echo -en "\033]1;Terminal\007"'

# # mount shared folder
# a alex='sudo mount -t vboxsf AlexShared /home/alex/shared'
# a unalex='sudo umount -a -t vboxsf'

# use meld as diff tool
# a xdiff='/usr/bin/meld'

# #rabbitmq stuff
# a amqp_q='sudo rabbitmqctl list_queues'
# a amqp_e='sudo rabbitmqctl list_exchanges'
# a amqp_s='sudo rabbitmqctl status'
#
# a be='bundle exec'
#
# # gem install
# a get_gem='sudo gem install'
# # gem install without rdoc and ri
# function get_gem_nr() {
#    sudo gem install $* --no-rdoc --no-ri
# }

# # ack-grep
# a agrep='ack-grep'
#
# #other misc
# a atest='RSPEC=true autotest'
#
# # RVM related
# a r187='rvm use 1.8.7'
# a r192='rvm use 1.9.2'
# a r193='rvm use 1.9.3'
