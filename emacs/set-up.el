;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; How to debug a .emacs file?      ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;
;   First start Emacs with the "-q" command line option.  Then, in the
;   *scratch* buffer, type the following:
;
;   (setq debug-on-error t)
;   (load-file "~/.emacs")
;
;   Then hit C-j to start loading .emacs file
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Some key bindings I like
(global-set-key "\C-c#" 'goto-line)
(global-set-key "\C-c=" 'what-line)

(put 'scroll-left 'disabled nil)

;; I don't like scrolling bars on the left side, so...
(set-scroll-bar-mode 'right)

;; Turn on horizontal scrolling with mouse wheel
(global-set-key (kbd "<mouse-6>") 'scroll-right)
(global-set-key (kbd "<mouse-7>") 'scroll-left)

;; Interactively Do Things
(require 'ido)
;; Somehow I don't like it on by default
;;(ido-mode t)

;; Rinari
(add-to-list 'load-path "~/workpad/emacs/rinari")
(require 'rinari)

;;; rhtml-mode
(add-to-list 'load-path "~/workpad/emacs/rhtml")
(require 'rhtml-mode)
  (add-hook 'rhtml-mode-hook
   (lambda () (rinari-launch)))

;------------------------------------------------------------------
; Following are DM stuff (current version: 2.3):
;------------------------------------------------------------------
(require 'dmacro)
(dmacro-load (concat (getenv "HOME") "/workpad/emacs/alex.dm") )
(global-set-key "\C-c(" 'dmacro-build)
(global-set-key "\C-c)" 'dmacro-save)
(setq auto-dmacro-alist (append '(("\\.el$"   . elisphead)
				  ("\\.pl$"   . header)
                                  ("\\.html$" . htmlhead)
				  ("\\.m$"    . header))
				  auto-dmacro-alist))

; load auto-indent-mode
(load-library "auto-indent")

;;;;;;;;;;;;;;;;;
;; Text mode
;;;;;;;;;;;;;;;;;
(add-hook 'text-mode-hook
  (function
   (lambda ()
     (setq tab-width 4)
     (setq tab-stop-list '(4 8 12 16 20 24 28 32 36 40 44 48 52 56 60 64 68 72 76 80))
     (setq indent-tabs-mode nil)
;     (define-key text-mode-map "\C-i" 'self-insert-command)
     )))

;;;;;;;;;;;;;;;;;
;; Perl Mode Hook
;;;;;;;;;;;;;;;;;
; autoload perl-mode
(autoload 'perl-mode "perl-mode" "Major mode for editing perl Scripts." t)
(add-hook 'perl-mode-hook
     (function
       (lambda ()
	 (setq comment-start "#")
         (setq last-edited-pattern "Last Modified: ")
	 (dmacro-load (concat (getenv "HOME") "/workpad/emacs/perl.dm") )
	 (local-set-key "\M-q"     'indent-region))))


;;;;;;;;;;;;;;;;;
;; Ruby Mode Hook
;;;;;;;;;;;;;;;;;
(add-hook 'ruby-mode-hook
     (function
       (lambda ()
	 (setq comment-start "#")
	 (dmacro-load (concat (getenv "HOME") "/workpad/emacs/ruby.dm") )
	 (require 'ruby-electric)
         (ruby-electric-mode t)
	 ;; Highlight matching blocks
	 (require 'ruby-block)
	 (ruby-block-mode t)
	 ;; do overlay
	 ;; (setq ruby-block-highlight-toggle 'overlay)
	 ;; display to minibuffer
	 ;; (setq ruby-block-highlight-toggle 'minibuffer)
	 ;; display to minibuffer and do overlay
	 (setq ruby-block-highlight-toggle t)
	 (local-set-key "\M-q"     'indent-region)
	 )))


;;; Tabbing (part of emacs-goodies-el package, located in
;;; /usr/share/emacs/site-lisp/emacs-goodies-el folder)
(require 'tabbar)
(tabbar-mode) ;comment out this line to start without the tab on top
(global-set-key [(control shift h)] 'tabbar-mode)
(global-set-key [(control shift up)] 'tabbar-backward-group)
(global-set-key [(control shift down)] 'tabbar-forward-group)
(global-set-key [(control shift left)] 'tabbar-backward)
(global-set-key [(control shift right)] 'tabbar-forward)
(global-set-key [(control next)] 'tabbar-forward-tab)
(global-set-key [(control prior)] 'tabbar-backward-tab)

(global-set-key [(control tab)] 'tabbar-last-selected-tab)

;;; Dired+
(require 'dired+)

(put 'dired-find-alternate-file 'disabled nil)

;; yasnippet stuff; I want that minor mode to be on globally
(add-to-list 'load-path "~/workpad/emacs/yasnippet")
(require 'yasnippet)
(yas/initialize)
(yas/load-directory "~/workpad/emacs/yasnippet/snippets")
(yas/global-mode t)
;; I also want the dropdown list extension
(require 'dropdown-list)
(setq yas/prompt-functions '(yas/dropdown-prompt
                             yas/ido-prompt
                             yas/completing-prompt))
;; ruby mode and ruby-electric minor mode messes up the TAB key 
(define-key ruby-mode-map (kbd "TAB") nil)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; \C-v and M-v with new functions
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
(global-set-key "\C-v"         'new-scroll-up)
(global-set-key "\M-v"         'new-scroll-down)
(define-key global-map [prior] 'new-scroll-down)
(define-key global-map [next]  'new-scroll-up)
(defun new-scroll-up (arg)
  (interactive "P")
  (if (eobp)
      (error "End of buffer"))
  (condition-case dummy
      (scroll-up arg)
    (error
     (goto-char (point-max)))))
(defun new-scroll-down (arg)
  (interactive "P")
  (if (bobp)
      (error "Beginning of buffer"))
  (condition-case dummy
      (scroll-down arg)
    (error
     (goto-char (point-min)))))

;;;;;;;;;;;;;;;;;;;;;
; Rectangle stuff
;;;;;;;;;;;;;;;;;;;;;
; Auto load the minor mode -- rect-mode, require rect-mode.el!
(autoload 'rect-mode "rect-mode" "Rectangle minor mode." t)
(global-set-key "\C-c%" 'rectangle-stuff)
(defun rectangle-stuff (arg)
  "Open, clear, kill, yank, delete a rectangular region with corners
   at point and mark."
  (interactive "cRectangle   o:open  c:clear  k:kill  y:yank  d:delete  r:copy-to-register")
  (cond
    ((eq arg ?o) (call-interactively 'open-rectangle))
    ((eq arg ?c) (call-interactively 'clear-rectangle))
    ((eq arg ?k) (call-interactively 'kill-rectangle))
    ((eq arg ?y) (call-interactively 'yank-rectangle))
    ((eq arg ?d) (call-interactively 'delete-rectangle))
    ((eq arg ?r) (call-interactively 'copy-rectangle-to-register))
   (t (message "Undefined ..."))))

;; A smart kill line:
(global-set-key "\C-k"  'smart-kill-line)
(defun smart-kill-line (&optional count)
  "Front end to kill-line: if at the beginning of the line, kill the entire
thing, else kill N lines and remove leading whitespace from the newly-joined
line."
  (interactive "*P")
  (cond ((bolp)
         (kill-line (if (null count) 1 count)))
        (t
         (kill-line count)
         (cond ((and (not (bolp)) (not (eolp)))
                (delete-region (point)
                               (progn
                                 (skip-chars-forward " \t")
                                 (point)))
                (if (and (not (eolp))
                         (not (memq (preceding-char) '(? ?\t))))
                    (insert ? )))))))

;;Coffee mode set up
(add-to-list 'load-path "~/workpad/emacs")
(require 'coffee-mode)

;;rdebug stuff
(add-to-list 'load-path "~/workpad/emacs/rdebug")
(require 'rdebug)
