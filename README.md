Setup Your Workspace With __Workpad__
=====================================

You should clone this workpad git repository from your home directory.
```
$ cd ~
$ git clone git@gitlab.com:alexw668/workpad.git
```

bash
----

The .bashrc file in your home directory should only contain machine specific settings; for global settings, add the following line in it:

<pre>
if [ -f ~/workpad/bashrc ]; then
	 . ~/workpad/bashrc
fi      
</pre>

zsh
-----

Create soft symbolic links for .zshrc and .zshenv in your home directory:
```
ln -s ~/workpad/zsh/zshrc ~/.zshrc
ln -s ~/workpad/zsh/zshenv ~/.zshenv

```

Emacs
-----

If you don't have .emacs file in your home directory, simply make a symbolic link to use the one setup in workpad

<pre>
% ln -s ~/workpad/emacs/emacs.el .emacs
</pre>

If you already have .emacs file in your home directory, you should only need the following lines (in addition to some optin settings)

<pre>
(setq load-path (cons (expand-file-name 
 (concat (getenv "HOME") "/workpad/emacs")) load-path))

(load "set-up")
</pre>

Git
---

Depending on what OS you are dealing with, make a symbolic link from your home directory. The `wordpad` repo
has three versions: 

| OS               | git config file   |
| ---              | ----------        |
| Windows 10 WSL   | gitconfig.win10   |
| Linux            | gitconfig         |
| Mac OS           | gitconfig.mac     |


<pre>
$ ln -s ~/workpad/gitconfig ~/.gitconfig
</pre>

Vim && NeoVim
-------------

For neovim:

* Ensure existence of directory $HOME/.config/nvim and autoload subfolder:
   
```
$ mkdir -p ~/.config/nvim/autoload

```

* Create a symbolic link in ~/.config/nvim folder:

```
$ ln -s ~/workpad/vim/init.vim ~/.config/nvim/init.vim
```

For vim:

* Ensure existence of directory $HOME/.vim and autoload subfolder:

```
$ mkdir -p ~/.vim/autoload

```

* Install vim-plug (in case the auto install in vim-plug/vim-plugins.vim does not work)
    ```
curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
    ```

* Create .vimrc that only contains the following line:

```
source ~/workpad/nvim/init.vim
```

Some Default hidden (e.g, Run Command) files 
--------------------------------------------

Workpad has some "dot" files that can be used as default rc files (such as .irbrc, .rspec) or configuration files (e.g., .gitconfig).
If you like any of them, you can simply create a symbolic link pointing to the one. For example, I set up the following

<pre>
$ cd ~
$ ln -s ~/workpad/irbrc .irbrc
$ ln -s ~/workpad/rspec .rspec
$ ln -s ~/workpad/gemrc .gemrc
</pre>

## Does GitLab support MathJax?

$$
\boldsymbol{W} = 
 \begin{bmatrix}
  w_{11} &  0 &  \cdots & 0 & 0\\
  0 & w_{11} &  \cdots  & 0 & 0 \\
  \vdots & \vdots & \ddots & \vdots & \vdots\\
  0 & \cdots  & 0 & w_{nn} & 0\\
  0 & \cdots  & 0 & 0 & w_{nn}
 \end{bmatrix}
$$
