#!/usr/local/bin/perl -- # -*-Perl-*-
#------------------------------------------------------------------
# Filename:      texcln.pl
# Created on:    Thu Feb  9 17:37:31 1995
# By:            Liyun Wang, NASA/GSFC
# Last Modified: Fri Feb 10 09:55:31 1995 (lwang@achilles.nascom.nasa.gov)
#------------------------------------------------------------------
#
# Perpose:
#     Clean TeX associated files
# 
# Modification History:
#     Version 1, created, Liyun Wang, NASA/GSFC, February 9, 1995

@extlist = ('aux', 'bbl', 'blg', 'dvi', 'lof', 'log', 'lot', 'ps', 'toc');
#
# Add more to the extension list if you wish.
#

&usage unless (@ARGV);

if ($ARGV[0] =~ /^-/) {
    $o = shift @ARGV;
    if ($o =~ /^-e/) {$basext = shift @ARGV;} else {
	die "Invalid option: $o\n";
    }
}

$basext = 'tex' unless (defined($basext));

&usage unless (@ARGV);

foreach $file (@ARGV) {
    ($path, $base, $ext) = &splitfn($file);
    if ($ext ne "") {$master_file = $base . ".$ext";} else {
	$master_file = $base . ".$basext";
    }
    if (-e $master_file) {
	print "Cleaning \033[1m$base\033[0m .... ";
	local(@kill_list);
	foreach (@extlist) {
	    push(@kill_list, "$base.$_");
	}
	$num = unlink(@kill_list);
	if ($num > 1) {
	    print "$num files deleted.\n";
	} elsif ($num == 1) {
	    print "$num file deleted.\n";
	} else {
	    print "No file is deleted.\n";
	}
    } else {
	print "\033[1m$master_file\033[0m not found.\n";
    }
}
print "Done.\n";

sub usage {
    print "texcln v. 2.0                       (c) 1992, 1995 Liyun Wang\n\n";
    print "\033[1mtexcln\033[0m cleans TeX or LaTeX  documents if found in the current\n";
    print "directory. All files with extensions \033[1maux, bbl, blg, lof, lot,\n";
    print "ps, toc\033[0m associated with the root filename(s) will be erased.\n\n";
    print "      Usage: texcln [-e ext] root_filename(s)\n\n";
    print "where 'ext' is  the file extension of TeX  document (default: tex).\n";
    exit;
}

sub splitfn {
#------------------------------------------------------------------
#   Split filename into path, basename (excluding extension), and 
#   extension.
#
#   Usage: ($path, $basename, $ext) = &splitfn('file_name');
#------------------------------------------------------------------
    local ($filename, $defext) = @_;
    local ($path, $basename, $ext) = ("", "", "");

    $filename =~ tr/\\/\//;    # translate \ into /

    $filename = $filename . $defext if ! -r $filename;
    
    if ($filename =~ /\//) {
	($path = $filename) =~ s/\/[^\/]*$//;
	($basename = $filename) =~ s/.*\///;
    }
    else {
	$path = ".";
	$basename = $filename;
    }

    if ($basename =~ /\./) {
	($ext = $basename) =~ s/.*\.//;
	$basename =~ s/\.[^.]*$//;
    }

    ($path . "/",$basename,$ext);
}

