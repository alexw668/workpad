" auto-install vim-plug

 if has('nvim')
 "============== For NeoVim
  " Make sure directory ~/.config/nvim/autoload is created
  " $ mkdir -p ~/.config/nvim/autoload

  let s:autoload_path = '~/.config/nvim/autoload/plug.vim'
  let s:plugged_path = '~/.config/nvim/autoload/plugged'
else
  "============== For Vim
  " Make sure directory ~/.vim/autoload and ~/.vim/plugged are created
  " $ mkdir -p ~/.vim/autoload
  " $ mkdir -p ~/.vim/plugged
  let s:autoload_path = '~/.vim/autoload/plug.vim'
  let s:plugged_path = '~/.vim/plugged'
endif

if empty(glob(s:autoload_path))
  silent !curl -fLo s:autoload_path --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  "autocmd VimEnter * PlugInstall
  autocmd VimEnter * PlugInstall | source $MYVIMRC
endif

call plug#begin(s:plugged_path)
  "{{ The Basics }}
      " Plug 'gmarik/Vundle.vim'                           " Vundle
      Plug 'itchyny/lightline.vim'                       " Lightline statusbar
      Plug 'suan/vim-instant-markdown', {'rtp': 'after'} " Markdown Preview
      Plug 'frazrepo/vim-rainbow'
  "{{ File management }}
      " Plug 'vifm/vifm.vim'                               " Vifm
      Plug 'preservim/nerdtree'                          " Nerdtree
      Plug 'tiagofumo/vim-nerdtree-syntax-highlight'     " Highlighting Nerdtree
      Plug 'ryanoasis/vim-devicons'                      " Icons for Nerdtree
  "{{ Productivity }}
      " Plug 'vimwiki/vimwiki'                             " VimWiki 
      Plug 'jreybert/vimagit'                            " Magit-like plugin for vim
  "{{ Tim Pope Plugins }}
      Plug 'tpope/vim-surround'                          " Change surrounding marks
  "{{ Syntax Highlighting and Colors }}
      " Plug 'PotatoesMaster/i3-vim-syntax'                " i3 config highlighting
      Plug 'vim-python/python-syntax'                    " Python highlighting
      Plug 'ap/vim-css-color'                            " Color previews for CSS

      Plug 'junegunn/vim-emoji'                          " Vim needs emojis!
      Plug 'vim-utils/vim-man'

      " Better Syntax Support
      Plug 'sheerun/vim-polyglot'
      " Auto pairs for '(' '[' '{'
      Plug 'jiangmiao/auto-pairs'  

    " Default theme
      Plug 'joshdick/onedark.vim'

      " Which key
      Plug 'liuchengxu/vim-which-key'

      Plug 'vim-airline/vim-airline'
      Plug 'vim-airline/vim-airline-themes'       
      Plug 'vim-scripts/AutoComplPop'

      " range plugin
      " Plug 'kevinhwang91/rnvimr', {'do': 'make sync'}
 
      " fzf-vim plugin
      Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
      Plug 'junegunn/fzf.vim'
      Plug 'airblade/vim-rooter'

      " Tabular and Markdown 
      " Plug 'godlygeek/tabular' 
      Plug 'tpope/vim-markdown' 
      if has('nvim')
         Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
         Plug 'mhinz/vim-startify'
      endif

      Plug 'dhruvasagar/vim-table-mode'

      " surround plugins
      Plug 'tpope/vim-surround'

      Plug 'machakann/vim-highlightedyank'

      call plug#end()

" Automatically install missing plugins on startup
autocmd VimEnter *
  \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  \|   PlugInstall --sync | q
  \| endif
