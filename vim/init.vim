" Init file for Vim or NeoVim

"========== For vim 
" You should either source this file in your ~/.vimrc or make ~/.vimrc
" as a symblic link pointing to this file
"
"========== For NeoVim 
" What you should do is to create a symbolic link in ~/.config/nvim/init.vim 
" pointing to this file. For example:
" $ ln ~/workpad/vim/init.vim ~/.config/nvim/init.vim

source $HOME/workpad/vim/vim-plug/vim-plugins.vim
source $HOME/workpad/vim/general/settings.vim
source $HOME/workpad/vim/keys/mappings.vim

source $HOME/workpad/vim/themes/onedark.vim
source $HOME/workpad/vim/themes/airline.vim

" source $HOME/workpad/vim/vim-plug/netrw.vim
source $HOME/workpad/vim/plug-config/nerdtree.vim

source $HOME/workpad/vim/plug-config/autocomplete.vim

" load ranger plugin setting
" source $HOME/workpad/vim/plug-config/rnvimr.vim

" load settings for fzf-vim plugin
source $HOME/workpad/vim/plug-config/fzf.vim

" load markdown settings
source $HOME/workpad/vim/plug-config/markdown.vim

" loadup settings for startify
source $HOME/workpad/vim/plug-config/startify.vim
