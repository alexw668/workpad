#!/usr/bin/env bash
for file in ~/workpad/{aliases,functions}; do
	[ -r "$file" ] && [ -f "$file" ] && source "$file";
done;
unset file;

if [[ -n "$IS_WSL" || -n "$WSL_DISTRO_NAME" ]]; then
	# Windows 10 WSL
    source ~/workpad/aliases.win10
fi

if [[ $OSTYPE == darwin* ]]; then
   echo "This is MaxOS"
fi

# enable programmable completion features (you don't need to enable
# this, if it's already enabled in /etc/bash.bashrc and /etc/profile
# sources /etc/bash.bashrc).
if [ -f /etc/bash_completion ] && ! shopt -oq posix; then
    . /etc/bash_completion
fi

prepend_path $HOME/workpad/perl
prepend_path $HOME/workpad/bash

# Identify OS and Machine -----------------------------------------
export OS=`uname -s | sed -e 's/ */-/g;y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/'`
export OSVERSION=`uname -r`; OSVERSION=`expr "$OSVERSION" : '[^0-9]*\([0-9]*\.[0-9]*\)'`
export MACHINE=`uname -m | sed -e 's/ */-/g;y/ABCDEFGHIJKLMNOPQRSTUVWXYZ/abcdefghijklmnopqrstuvwxyz/'`
export PLATFORM="$MACHINE-$OS-$OSVERSION"

export USERNAME=``

# History ----------------------------------------------------------
export HISTCONTROL=ignoredups
#export HISTCONTROL=erasedups
export HISTFILESIZE=3000
export HISTIGNORE="ls:cd:[bf]g:exit:..:...:ll:lla:cls:h"

#highlight bash (http://osxdaily.com/2012/02/21/add-color-to-the-terminal-in-mac-os-x/)
export CLICOLOR=1
export LSCOLORS=GxFxCxDxBxegedabagaced

alias h=history
hf(){
  grep "$@" ~/.bash_history
}

########
# RUBY #
########
# really awesome function, use: cdgem <gem name>, cd's into your gems directory
# and opens gem that best matches the gem name provided
function cdgem {
  cd `gem env gemdir`/gems
  cd `ls | grep $1 | sort | tail -1`
}
function gemdoc {
  GEMDIR=`gem env gemdir`/doc
  open $GEMDIR/`ls $GEMDIR | grep $1 | sort | tail -1`/rdoc/index.html
}
alias rurl="rake routes | sed -e '1d;s,^[^/]*,,;s, .*,,' | sort | uniq"

# PWS
export PWS="$HOME/Documents/Dropbox/MyDocs/.safe/pws"
alias pw='pws'
