###############################################################
# Filename:    alex.dm
# Last Modified: Wed Mar 21 21:17:03 2001 (Liyun@LIYUN)
###############################################################
#
# Macros for use with Dmacro

## Define an alias for the string indicating the current date
# ALIAS: today	(dmacro today-internal)
# ALIAS: my_email	(dmacro my_email_internal)
# ALIAS: organization (if (eval (getenv "ORGANIZATION")) (eval (concat ", " (getenv "ORGANIZATION"))))
## From now on you can use ~(today) as a directive of Dmacro.

today-internal
~((month) :pad nil) ~((date) :pad nil), ~(year)
#

my_email_internal
alex_wang@hsihealth.com
#

#####################
# MODE: nil
#####################
#######
ttoday expand Expand time string
~((month) :pad nil) ~((date) :pad nil), ~(year)
#
#######
usa expand Expand USA string.
United States of America
#
#######
uk expand Expand UK string.
United Kingdom
#
#######
address expand Expand my work address
Health Services Integration
416 B Street, Suite B
Santa Rosa, CA 95401
(707)303-4416
#
#######
current_filename expand Insert current file name.
~(file)
#
#######
me expand Insert my full name as defined by the system
~(user-name)~(organization)
#
#######
today expand Return today's date
~(today)
#
#######
jan expand Expand January string
January
#
#######
feb expand Expand February string
February
#
#######
mar expand Expand March string
March
#
#######
apr expand Expand April string
April
#
#######
aug expand Expand August string
August
#
#######
sep expand Expand September string
September
#
#######
oct expand Expand October string
October
#
#######
nov expand Expand November string
November
#
#######
dec expand Expand December string
December
#


#######
# MODE:	csh-mode
#######
header expand Insert a standard header on the C shell script document
\#!/bin/csh -f
\#------------------------------------------------------------------
\# Filename:      ~(file)
\# Created on:    ~(chron)
\# By:            ~(user-name)~(organization)
\# Last Modified~(mark):
\#------------------------------------------------------------------
\#
~(point)

\# Local Variables:
\# mode: (csh)
\# End:

#
#######
comment expand Insert commentary structure in C shell script
\#------------------------------------------------------------------
\# ~(point)
\#------------------------------------------------------------------

#
#######
foreach indent Insert the foreach .. end structure in C shell script
foreach ~(prompt variable) (~point)
~(mark)
end

#
#######
case indent Insert case .. breaksw structure in C shell script
case ~point:
~mark
breaksw

#
#######
switch indent Insert switch .. endsw structure in C shell script
switch (~point)
   case  ~mark:

   breaksw
   default:
   breaksw
endsw

#
#######
if indent Insert if ... else ... endif structure in C shell script
if (~(prompt condition)) then
   ~(point)
endif

#
#######
ifelse indent Insert if ... else ... endif structure in C shell script
if (~(prompt condition)) then
   ~(point)
else
   ~(mark)
endif

#
#######
while indent Insert while ... end structure in C shell script
while (~(prompt condition))
   ~(point)
end

#

####################################################################
## End of "alex.dm"
####################################################################
