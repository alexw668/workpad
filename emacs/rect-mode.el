;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Filename: rect-mode.el                                       ;
; Created on: Sun May 23 22:44:15 1993                         ;
; By:         Liyun Wang, Dept. of Astronomy, Univ. of Florida ;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; Last Edited: Sun May 23 22:44:33 1993 (lwang@elc3.astro.ufl.edu)
;; $modified: Fri May 21 17:46:01 1993 by rshouman $
;; description: provide a minor mode for killing and yanking rectangles with
;; the same key bindings usually used for killing and yanking regions.
;;
;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 2 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program; if not, write to the Free Software
;; Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;;
;; file: rect-mode.el
;; author: Radey Shouman <rshouman@chpc.utexas.edu>
;;
;; LCD Archive Entry:
;; rect-mode|Radey Shouman|rshouman@chpc.utexas.edu|
;; provide a minor mode for killing and yanking rectangles.|
;;     (suggested)
;;
;; Installation: byte-compile it,
;; (autoload 'rect-mode "rect-mode" "Rectangle minor mode." t)
;;          or
;; (require 'rect-mode)
;;
;; Usage: M-x rect-mode will toggle rect-mode -- in this mode, the keys
;;        usually bound to kill-region, copy-as-kill, yank, and yank-pop
;;        will instead kill, copy, or yank rectangles.  A rectangle kill
;;        ring is implemented.  This is not the same as the usual kill ring,
;;        something killed as a rectangle can only be yanked as a rectangle,
;;        something killed as a string can only be yanked as a string.
;;
;;        Rect-mode rebinds keys bound to the following functions when it is
;;        loaded:
;;
;;        kill-region kill-line yank copy-region-as-kill yank-pop forward-char
;;
;;        the functions themselves are not redefined.  If rect-mode should
;;        cause some problem, the key substitutions may be reversed with
;;        the function rect-unbind-keys.
;;
;; BUGS:  rect-yank and rect-yank-pop do not do anything with numeric
;;        arguments, unlike yank and yank-pop.  If you use this feature
;;        with yank, feel free to implement it for rect-mode as well.
;;
;;
;;        There isn't any convenient way to get to the kill-ring when
;;        in rect-mode, or rect-kill-ring when not in rect-mode.  It would be
;;        possible to combine the two rings by writing a yank function that
;;        would yank either strings or rectangles (lists).  I don't think this
;;        would be a good idea, it could be very confusing to use.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defvar rect-mode nil
  "Non-nil if the keys normally bound to kill-region and yank-region should
instead kill-rectangle and yank-rectangle. ")

(defvar rect-kill-ring-max 10)
(defvar rect-kill-ring nil)
(defvar rect-kill-ring-pointer nil)

(defun rect-mode (arg)
  "Minor mode for killing, copying, and yanking rectangles, rather than
strings.  Without prefix arg, toggle rect-mode, with positive numeric arg,
turn rect-mode on, with nonpositive numeric arg, turn rect-mode off. "
  (interactive "P")
  (make-variable-buffer-local 'rect-mode)
  (setq rect-mode
	(if (null arg) (not rect-mode)
	  (> (prefix-numeric-value arg) 0)))
  (if rect-mode
      (progn
	(or (assq 'rect-mode minor-mode-alist)
	    (setq minor-mode-alist
		  (cons '(rect-mode " Rect") minor-mode-alist))))
    (or buffer-read-only (picture-clean))))

(defun rect-kill-region (start end)
  "If in rect-mode, kill the current region and push it on the rectangle
kill ring, else call kill-region.  See also the function kill-region. "
  (interactive "*r")
  (if rect-mode
      (progn
	(if (> (point) (mark))
	    (exchange-point-and-mark))
	(setq rect-kill-ring
	      (cons (delete-extract-rectangle start end) rect-kill-ring))
	(if (> (length rect-kill-ring) rect-kill-ring-max)
	    (setcdr (nthcdr (1- rect-kill-ring-max) rect-kill-ring) nil))
	(setq rect-kill-ring-pointer rect-kill-ring))
    (kill-region start end)))

(defun rect-copy-as-kill (start end)
  "If in rect-mode, copy the current region to the rectangle kill ring,
else call copy-region-as-kill.  See also the function copy-region-as-kill. "
  (interactive "r")
  (if rect-mode
      (progn
	(setq rect-kill-ring
	      (cons (extract-rectangle start end) rect-kill-ring))
	(if (> (length rect-kill-ring) rect-kill-ring-max)
	    (setcdr (nthcdr (1- rect-kill-ring-max) rect-kill-ring) nil))
	(setq this-command 'rect-kill)
	(setq rect-kill-ring-pointer rect-kill-ring))
    (copy-region-as-kill start end)))

(defun rect-kill-line (&optional arg)
  "If in rect-mode, kill the rest of the current line as a rectangle,
with numeric arg, kill arg lines.  If not in rect-mode, call kill-line.
See also the function kill-line. "
  (interactive "*P")
  (if rect-mode
      (progn
	(let ((start (point))
	      (end (progn
		     (end-of-line (if arg (prefix-numeric-value) 1))
		     (point))))
	  (if (/= start end)
	      (rect-kill-region start end)
	    (delete-char 1))))
    (kill-line arg)))

(defun rect-yank (&optional arg)
  "If in rect-mode, yank the most recently killed or yanked rectangle, else
call yank.  See also the function yank. "
  (interactive "*P")
  (if rect-mode
      (progn
	(set-mark (point))
        (insert-rectangle (car rect-kill-ring-pointer))
	(if arg (exchange-point-and-mark)))
    (yank arg)
    (setq this-command 'yank)))

(defun rect-yank-pop (arg)
  "If in rect-mode, replace the just-yanked rectangle with a different
rectangle, else call yank-pop.  See also the function yank-pop. "
  (interactive "*p")
  (if rect-mode
      (progn
	(if (not (eq last-command 'rect-yank))
	    (error "Previous command was not a rect-yank"))
	(exchange-point-and-mark)
	(delete-rectangle (point) (mark))
	(set-mark (point))
	(setq rect-kill-ring-pointer
	      (or (cdr rect-kill-ring-pointer) rect-kill-ring))
	(insert-rectangle (car rect-kill-ring-pointer))
	(setq this-command 'rect-yank))
    (yank-pop arg)
    (setq this-command 'yank)))

(defun rect-forward-char (&optional arg)
  "Acts like forward-char, except that if eoln is reached in rect-mode,
spaces will be quietly added, these will be removed on leaving rect-mode.
See also the function forward-char. "
  (interactive "p")
  (or arg (setq arg 1))
  (if rect-mode
      (progn
	(while (and (> arg 0) (not (eolp)))
	  (progn
	    (forward-char 1)
	    (setq arg (1- arg))))
	(while (> arg 0)
	  (progn
	    (insert " ")
	    (setq arg (1- arg)))))
    (forward-char arg)))

(defun rect-unbind-keys ()
  "Reverse the key bindings made by rect-mode.  Should only be necessary
if there is some problem with rect-mode. "
  (interactive)
  ;; Make sure no buffers are in rect-mode.
  (mapcar (function
	   (lambda (buf)
	     (save-excursion (set-buffer buf) (rect-mode 0))))
	  (buffer-list))
  (substitute-key-definition 'rect-kill-region 'kill-region global-map)
  (substitute-key-definition 'rect-kill-line 'kill-line global-map)
  (substitute-key-definition 'rect-yank 'yank global-map)
  (substitute-key-definition 'rect-copy-region-as-kill 'copy-as-kill esc-map)
  (substitute-key-definition 'rect-yank-pop 'yank-pop esc-map)
  (message "rect-mode nuked"))

;; Stolen from picture.el
(or (fboundp 'picture-clean)
    (defun picture-clean ()
      "Eliminate whitespace at ends of lines."
      (save-excursion
	(goto-char (point-min))
	(while (re-search-forward "[ \t][ \t]*$" nil t)
	  (delete-region (match-beginning 0) (point))))))

(substitute-key-definition 'kill-region 'rect-kill-region global-map)
(substitute-key-definition 'kill-line 'rect-kill-line global-map)
(substitute-key-definition 'yank 'rect-yank global-map)
(substitute-key-definition 'copy-region-as-kill 'rect-copy-as-kill esc-map)
(substitute-key-definition 'yank-pop 'rect-yank-pop esc-map)
(substitute-key-definition 'forward-char 'rect-forward-char global-map)

(provide 'rect-mode)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; End of 'rect-mode.el'.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
