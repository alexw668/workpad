" Better nav for omnicomplete
inoremap <expr> <c-j> ("\<C-n>")
inoremap <expr> <c-k> ("\<C-p>")

" Enable/diable auto comment
map <leader>c :setlocal formatoptions-=cro<CR>
map <leader>C :setlocal formatoptions=cro<CR>

" Ctrl+e to back to normal mode
inoremap <silent> <C-e> <Esc>

"" Make adjusing split sizes a bit more friendly
noremap <silent> <C-Left> :vertical resize +3<CR>
noremap <silent> <C-Right> :vertical resize -3<CR>
noremap <silent> <C-Up> :resize +3<CR>
noremap <silent> <C-Down> :resize -3<CR>

nnoremap <leader>ghw :h <C-R>=expand("<cword>")<CR><CR>
nnoremap <leader>prw :CocSearch <C-R>=expand("<cword>")<CR><CR>
nnoremap <leader>bs /<C-R>=escape(expand("<cWORD>"), "/")<CR><CR>
nnoremap <leader>h :wincmd h<CR>
nnoremap <leader>j :wincmd j<CR>
nnoremap <leader>k :wincmd k<CR>
nnoremap <leader>l :wincmd l<CR>
nnoremap <leader>u :UndotreeShow<CR> " for git undo tree

nnoremap <leader>pv :wincmd v<bar> :Ex<bar> :vertical resize 30<CR>
nnoremap <leader>ps :Rg<Space> 

" source vim file
nnoremap <leader>sv :so $MYVIMRC<CR>

nnoremap <c-u> <ESC>viwUi
nnoremap <c-u> viwU<Esc>

" Better window navigation CTRL + hjkl
nnoremap <C-h> <C-w>h
nnoremap <C-j> <C-w>j
nnoremap <C-k> <C-w>k
nnoremap <C-l> <C-w>l

" Seamlessly treat visual lines as actual lines when moving around.
noremap j gj
noremap k gk
noremap <Down> gj
noremap <Up> gk
inoremap <Down> <C-o>gj
inoremap <Up> <C-o>gk

" tab navigation
nnoremap <TAB> gt
nnoremap <S-TAB> gT
" in case TAB key is remapped (like in markdown mode)
nnoremap <M-h> gT
nnoremap <M-l> gt

" TAB in general mode will move to text buffer
nnoremap <leader>bn :bnext<CR>
nnoremap <leader>bp :bprevious<CR>

" Toggle spell check.
map <F5> :setlocal spell!<CR>

" Toggle relative line numbers and regular line numbers.
nmap <F6> :set invrelativenumber<CR>

" Automatically fix the last misspelled word and jump back to where you were.
"   Taken from this talk: https://www.youtube.com/watch?v=lwD8G1P52Sk
nnoremap <leader>sp :normal! mz[s1z=`z<CR>

" Toggle visually showing all whitespace characters.
noremap <F7> :set list!<CR>
inoremap <F7> <C-o>:set list!<CR>
cnoremap <F7> <C-c>:set list!<CR>

"Better tap navigation
nnoremap <C-Left> :tabprevious<CR>
nnoremap <C-Right> :tabnext<CR>
nnoremap <silent> <A-Left> :execute 'silent! tabmove ' . (tabpagenr()-2)<CR>
nnoremap <silent> <A-Right> :execute 'silent! tabmove ' . (tabpagenr()+1)<CR>

" Alternative ways to save/quit
inoremap <silent> <C-s> <Esc>:w<CR>
inoremap <silent> <M-s> <Esc>:wq!<CR>
inoremap <silent> <C-q> <Esc>:q<CR>
inoremap <silent> <M-q> <Esc>:q!<CR>
 
" Copy and paste
vnoremap <C-c> "+y
vnoremap <C-x> "+x
" C-v may not work well from within the Windows terminal
inoremap <C-p> <Esc>"+gP
nnoremap <C-p> "+gP

"Alternate way to save
nnoremap <C-s> :w<CR>
" Alternate way to save and quit
nnoremap <M-s> :wq!<CR>
" Alternate way to quit: alt-q
nnoremap <C-q> :q<CR>
nnoremap <M-q> :q!<CR>

" <TAB>: completion.
inoremap <expr><TAB> pumvisible() ? "\<C-n>" : "\<TAB>"

" remap visual block mode (because in WSL, C-v is taken)
nnoremap <C-b> <C-v>

" Move 1 more lines up or down in normal and visual selection modes.
nnoremap K :m .-2<CR>==
nnoremap J :m .+1<CR>==
vnoremap K :m '<-2<CR>gv=gv
vnoremap J :m '>+1<CR>gv=gv

" Better tabbing
vnoremap < <gv
vnoremap > >gv

" Toggle on/off centering cursor 
nnoremap <leader>ss :let &scrolloff=999-&scrolloff<CR>

" Change 2 split windows from vert to horiz or horiz to vert
map <Leader>th <C-w>t<C-w>H
map <Leader>tk <C-w>t<C-w>K

nnoremap <Leader>o o<Esc>^Da
nnoremap <Leader>O O<Esc>^Da

function! ToggleLineNumber()
  if v:version > 703
    set norelativenumber!
  endif
  set nonumber!
endfunction
map <leader>r :call ToggleLineNumber()<CR>

" Toggle line wrapping
function ToggleWrap()
 if (&wrap == 1)
   set nowrap
 else
   set wrap
 endif
endfunction
map <leader>w :call ToggleWrap()<CR>

