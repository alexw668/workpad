class CleanUp
  
  def self.show_collections
    Company.database.collections.each do |coll|
      puts "#{coll.name}"
    end
  end

  def self.clean_amazon_records
    db = Company.database

    # companies 
    Company.all.each do |company|
      company.delete if company.source_system_id == 'hsi' && company.is_customer
    end

    # Other entities
    collections = ['profit_centers', 'patients', 'billing_records', 'billing_record_notes'].each do |coll_name|
      coll = db.collection(coll_name)
      coll.remove({"source_system_id"=>"hsi"})
    end
  end

  def self.clean_vh_billing_records
    db = BillingRecord.database
    br_coll = db.collection('billing_records')
    bh_coll = db.collection('billing_record_notes')    

    br_coll.remove({"source_system_id"=>"vh"})
    bh_coll.remove({"source_system_id"=>"vh"})
  end

  def self.display(source_system_id="vh")
    db = BillingRecord.database
    br_coll = db.collection('billing_records')
    bh_coll = db.collection('billing_record_notes')    
    
    puts "Billing Records:"
    br_coll.find({"source_system_id"=>source_system_id}).limit(10).each do |record|
      puts "billing record number: #{record['billing_record_number']}"
    end

    puts "Billing Record Notes:"
    bh_coll.find({"source_system_id"=>source_system_id}).limit(10).each do |record|
      puts "note for #{record['billing_record_number']}: #{record['body']}"
    end
  end
end
